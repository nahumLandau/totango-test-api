const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  isbn: {
    type: Number,
    required: true
  },
  pubDate: {
    type: Date,
    default: Date.now,
    required: true
  },
  genre: {
    type: String,
    enum: [
      "Science fiction",
      "Satire",
      "Drama",
      "Action",
      "Romance",
      "Mystery",
      "Horror"
    ],
    required: true
  }
});

BookSchema.index({ isbn: 1 });
const Books = mongoose.model("Book", BookSchema);

module.exports = {
  Books
};
