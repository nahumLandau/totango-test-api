/**
 * some express configuration
 */

// express
const express = require("express");
const app = express();

// config file (custom)
const config = require("./config");

// mongoose
const mongoose = require("mongoose");
mongoose.connect(config.db.mongo.uri);
mongoose.Promise = require("bluebird");

// books model
const { Books } = require("./books.model");

// db connection
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));

// body parser
var bodyParser = require("body-parser");
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true
  })
);

/**
 * Routes
 *
 * each route here behave as a end point
 * and get/set data from DB.
 *
 * because is a test and small app, all the methods
 * was wrote inside the route and not in a different
 * file
 */

// enable cross origin to all requests
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

// get all books list
app.get("/", function(req, res) {
  const allBooks = Books.find({})
    .lean()
    .exec((err, booksList) => {
      res.json(booksList);
    });
});

// get specific book by isbn
app.get("/book/:id", function(req, res) {
  const book = Books.findOne({ _id: req.params.id })
    .lean()
    .exec((err, singleBook) => {
      res.json(singleBook);
    });
});

// add new book
app.post("/add-new-book", function(req, res) {
  const newBook = new Books({
    title: req.body.title,
    description: req.body.description,
    isbn: req.body.isbn,
    author: req.body.author,
    pubDate: new Date(req.body.pubDate.toString()),
    genre: req.body.genre,
    price: req.body.price
  });
  newBook.save(err => {
    if (err) {
      res.json({ error: "new book failed", text: err });
    } else {
      res.json({ message: "new book added successfuly." });
    }
  });
});

// remove specific book by id
app.post("/delete-book/:bookId", function(req, res) {
  const book = Books.deleteOne({ _id: req.params.bookId }, err => {
    if (err) {
      res
        .status(412)
        .json({
          message: "cannot remove this book",
          statusCode: 412,
          error: err
        });
    } else {
      res.json({ message: "the book was removed successfuly." });
    }
  });
});

app.listen(process.env.PORT || 4000);
