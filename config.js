module.exports = {
  db: {
    mongo: {
      uri:
        process.env.MONGO_URI ||
        "mongodb://nahum:nahum2017@ds139890.mlab.com:39890/totango-test", //only for this test is here, not for production 
      options: {
        useMongoClient: true,
        config: {
          autoIndex: false
        }
      },
      // Enable mongoose debug mode
      debug: process.env.MONGO_DEBUG
        ? process.env.MONGO_DEBUG === "true"
        : false
    }
  }
};
